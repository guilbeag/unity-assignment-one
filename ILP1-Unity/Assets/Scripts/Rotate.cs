﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float rotationRate;

    void Update()
    {
        transform.Rotate(new Vector2(0, rotationRate * Time.deltaTime));
    }   
}