﻿using System.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float speed;
    public float jump;
    float moveVelocity;
    bool grounded = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //jump
        if (Input.GetKeyDown(KeyCode.Space) ||
                Input.GetKeyDown(KeyCode.W))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(
                GetComponent<Rigidbody2D>().velocity.x, jump);
        }

        moveVelocity = 0;

        //movement(side to side)
        if (Input.GetKey(KeyCode.A) ||
            Input.GetKey(KeyCode.LeftArrow))
        {
            moveVelocity = -speed; //left
        }
        if (Input.GetKey(KeyCode.D) ||
           Input.GetKey(KeyCode.RightArrow))
        {
            moveVelocity = speed; //right
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity,
            GetComponent<Rigidbody2D>().velocity.y);
    }

}