﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    public float speed;
    public float jump;
    float moveVelocity;
    bool grounded = false;
    Rigidbody2D rigidbody2DComponent;
    private float thrust = 10.0f;

    // Start is called before the first frame update
    void Start()
    {

    }
    void Awake()
    {
        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //keep yourself down
        if (Input.GetKeyDown(KeyCode.S) ||
            Input.GetKeyDown(KeyCode.Space) ||
            Input.GetKeyDown(KeyCode.DownArrow))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(
                GetComponent<Rigidbody2D>().velocity.x, -jump);

        }

        moveVelocity = 0;

        //movement(side to side)
        if (Input.GetKey(KeyCode.A) ||
            Input.GetKey(KeyCode.LeftArrow))
        {
            moveVelocity = -speed; //left
        }
        if (Input.GetKey(KeyCode.D) ||
           Input.GetKey(KeyCode.RightArrow))
        {
            moveVelocity = speed; //right
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity,
            GetComponent<Rigidbody2D>().velocity.y);

        rigidbody2DComponent.AddForce(transform.up * thrust);
    }
    //picking up the coin
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Coin")
        {
            Destroy(other.gameObject);
        }
    }

    //killing the character if they touch the thorns
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Thorns")
        {
            Destroy(gameObject);
       }

    }
}